//
//  NYSchoolsApp.swift
//  NYSchools
//
//  Created by Vargese, Sangeetha on 3/15/23.
//

import SwiftUI

@main
struct NYSchoolsApp: App {
    @StateObject var viewModel = SchoolsViewModel()
    var body: some Scene {
        WindowGroup {
            SchoolsView().environmentObject(viewModel)
        }
    }
}
