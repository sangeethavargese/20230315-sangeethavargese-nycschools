//
//  SchoolsView.swift
//  NYSchools
//
//  Created by Vargese, Sangeetha on 3/14/23.
//

import SwiftUI

struct SchoolsView: View {
    @EnvironmentObject var viewModel: SchoolsViewModel
    @State private var error: NYSchoolsError?
    @State private var hasError = false
    @State private var searchText = ""
    @State var isLoading = false
    var body: some View {
        NavigationView {
            if isLoading {
                ProgressView("Loading..")
            } else {
                List(searchResults) { school in
                    NavigationLink(destination: SchoolsDetailView(id: school.id)) {
                        VStack(alignment: .leading) {
                            SchoolRowView(school: school)
                        }
                    }
                }.searchable(text: $searchText)
                    .alert(isPresented: $hasError, error: error) {}
                    .listStyle(.inset)
                    .navigationBarTitleDisplayMode(.inline)
                    .toolbar {
                        ToolbarItem(placement: .principal) {
                            Text("NYC High Schools").font(.title2).bold()
                        }
                    }
            }
        }
        .task {
            await fetchSchools()
        }
    }
    
    var searchResults: [School] {
        if searchText.isEmpty {
            return viewModel.schools
        } else {
            /// Adding search ability for schools by name
            return viewModel.schools.filter { $0.schoolName.contains(searchText) }
        }
    }
    
    func fetchSchools() async {
        isLoading = true
        do {
            try await viewModel.fetchSchools()
        } catch {
            self.error = error as? NYSchoolsError ?? .unexpectedError(error: error)
            self.hasError = true
        }
        isLoading = false
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolsView()
    }
}
