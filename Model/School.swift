//
//  School.swift
//  NYSchools
//
//  Created by Vargese, Sangeetha on 3/14/23.
//

import Foundation

struct School : Codable, Identifiable {
    var id: String
    let schoolName: String
    let overviewParagraph: String?
    let location: String?
    let phoneNumber: String?
    let fax_number: String?
    let schoolEmail: String?
    let website: String?
    let primaryAddressLine1: String?
    let city: String?
    let zip: String?

    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case location = "location"
        case phoneNumber = "phone_number"
        case fax_number = "fax_number"
        case schoolEmail = "school_email"
        case website = "website"
        case primaryAddressLine1 = "primary_address_line_1"
        case city = "city"
        case zip = "zip"
    }
}

