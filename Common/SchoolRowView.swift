//
//  SchoolRowView.swift
//  NYSchools
//
//  Created by Vargese, Sangeetha on 3/15/23.
//

import SwiftUI
struct SchoolRowView: View {
    var school: School
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(school.schoolName)
                Text(school.city ?? "").font(.subheadline).foregroundColor(.gray)
            }
        }
    }
}



