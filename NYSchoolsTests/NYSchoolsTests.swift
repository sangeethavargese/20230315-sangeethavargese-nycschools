//
//  NYSchoolsTests.swift
//  NYSchoolsTests
//
//  Created by Vargese, Sangeetha on 3/15/23.
//

import XCTest
@testable import NYSchools

final class NYSchoolsTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
   ///  Test  client fetch data correctly
    func testClientDoesFetchSchoolData() async throws {
        let downloader = TestDownloader()
        let client = NYSchoolsClient(downloader: downloader)
        let quakes = try await client.schools
        XCTAssertEqual(quakes.count, 2)
    }
}
