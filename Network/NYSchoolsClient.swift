//
//  NYSchoolsClient.swift
//  NYSchools
//
//  Created by Vargese, Sangeetha on 3/14/23.
//

import Foundation

private let feedURL = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!
private let feedURLScore = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")!

class NYSchoolsClient {
    
    var schools: [School] {
        get async throws {
            let data = try await downloader.httpData(from: feedURL)
            let allSchools = try decoder.decode([School].self, from: data)
            return allSchools
        }
    }
    
    private lazy var decoder: JSONDecoder = {
        let aDecoder = JSONDecoder()
        //aDecoder.keyDecodingStrategy = .convertFromSnakeCase
        return aDecoder
    }()
    
    var scores: [SatScore] {
        get async throws {
            let data = try await downloader.httpData(from: feedURLScore)
            let allScores = try JSONDecoder().decode([SatScore].self, from: data)
            return allScores
        }
    }
    
    private let downloader: any HTTPDataDownloader
    
    init(downloader: any HTTPDataDownloader = URLSession.shared) {
        self.downloader = downloader
    }
}


