//
//  NYSchoolsError.swift
//  NYSchools
//
//  Created by Vargese, Sangeetha on 3/14/23.
//

import Foundation

enum NYSchoolsError: Error {
    case missingData
    case networkError
    case unexpectedError(error: Error)
}

extension NYSchoolsError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .missingData:
            return NSLocalizedString("The data couldn’t be read because it is missing.", comment: "")
        case .networkError:
            return NSLocalizedString("Error fetching data over the network.", comment: "")
        case .unexpectedError(let error):
            return NSLocalizedString("Received unexpected error. \(error.localizedDescription)", comment: "")
        }
    }
}
