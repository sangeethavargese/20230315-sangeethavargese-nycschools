# 20230315-SangeethaVargese- NYCSchools

## NYC Schools
This is a simple application that shows data of high schools and the SAT scores.
Fetching the data from the APIs 
 List of high schools: https://dev.socrata.com/foundry/data.cityofnewyork.us/s3k6-pzi2 
 SAT Scores:  https://dev.socrata.com/foundry/data.cityofnewyork.us/f9bf-2cp4
The app has two screens, one is a list view of all schools, tapping on a row will take you to a detail screen. 
The detail screen shows the SAT scores, contact details of the school and an overview about the school.




