//
//  SchoolsViewModel.swift
//  NYSchools
//
//  Created by Vargese, Sangeetha on 3/14/23.
//

import Foundation

@MainActor
class SchoolsViewModel: ObservableObject {

    @Published var schools: [School] = []
    @Published var score: [SatScore] = []
    
    let client: NYSchoolsClient
    
    init(client: NYSchoolsClient = NYSchoolsClient()) {
        self.client = client
    }
    
    func fetchSchools() async throws {
        let allSchools = try await client.schools
        self.schools = allSchools
        /// Sorting in alphabetical order
        self.schools = self.schools.sorted(by: { $0.schoolName < $1.schoolName})
    }

    func fetchScore() async throws {
        let allScores = try await client.scores
        self.score = allScores
    }
}
