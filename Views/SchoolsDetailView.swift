//
//  SchoolsDetailView.swift
//  NYSchools
//
//  Created by Vargese, Sangeetha on 3/14/23.
//

import SwiftUI

struct SchoolsDetailView: View {
    @EnvironmentObject var viewModel: SchoolsViewModel
    @State private var error: NYSchoolsError?
    @State private var hasError = false
    @State var id: String
    
    var body: some View {
        VStack() {
                let fullData = viewModel.schools.first(where: {$0.id == id})
                Text(fullData?.schoolName ?? "").bold()
                    .font(.title2)
                List {
                    Section(header: Text("SAT Scores").bold()) {
                        if let scoreData = viewModel.score.first(where: {$0.id == id}) {
                            Text("SATScore for Math: \(scoreData.satMathAvgScore)")
                            Text("SATScore for Reading: \(scoreData.satCriticalReadingAvgScore)")
                            Text("SATScore for Writing: \(scoreData.satWritingAvgScore)")
                        }
                        else {
                            Text("SAT data not available for this school").foregroundColor(.red)
                        }
                    }
                    Section(header: Text("Contact").bold()) {
                        Text("Phone: \(fullData?.phoneNumber ?? "")")
                        Text("Email: \(fullData?.schoolEmail ?? "")")
                        Text("Website: \(fullData?.website ?? "")")
                        Text("Address: \(fullData?.primaryAddressLine1 ?? "")")
                    }
                    Section(header: Text("About Us").bold()) {
                        Text("\(fullData?.overviewParagraph ?? "")").font(.subheadline)
                    }
                }
                .listStyle(.insetGrouped)
        }.task {
            await fetchScore()
        }
    }
    
    func fetchScore() async {
        do {
            try await viewModel.fetchScore()
        } catch {
            self.error = error as? NYSchoolsError ?? .unexpectedError(error: error)
            self.hasError = true
        }
    }
}

struct SchoolDetails_Previews: PreviewProvider {
    static var previews: some View {
        SchoolsDetailView(id: "0")
    }
}
